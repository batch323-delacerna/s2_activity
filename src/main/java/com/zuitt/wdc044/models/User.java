package com.zuitt.wdc044.models;

import javax.persistence.*;
@Entity
//designate table via @Table
@Table(name="users")
public class User {
    // indicate that this property represents the primary key via @Id
    @Id
    //value for this property will be auto-incremented
    @GeneratedValue
    private Long id;

    //Class properties that represent table columns in a relational database are annotated as @Column
    @Column
    private String username;

    @Column
    private String password;

    //Default constructor
    public User(){}

    public User(String username, String password){
        this.username = username;
        this.password = password;
    }

    public String getUsername(){
        return username;
    }

    public void setUsername(String username){
        this.username = username;
    }

    public String getPassword(){
        return password;
    }

    public void setPassword(String password){
        this.password = password;
    }
}