package com.zuitt.wdc044.models;

import javax.persistence.*;

//mark this Java object as a representation of a database table via @Entity
@Entity
//designate table via @Table
@Table(name="posts")
public class Post {
    // indicate that this property represents the primary key via @Id
    @Id
    //value for this property will be auto-incremented
    @GeneratedValue
    private Long id;

    //Class properties that represent table columns in a relational database are annotated as @Column
    @Column
    private String title;

    @Column
    private String content;

    //Default constructor
    public Post(){}

    public Post(String title, String content){
        this.title = title;
        this.content = content;
    }

    public String getTitle(){
        return title;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public String getContent(){
        return content;
    }

    public void setContent(String content){
        this.content = content;
    }
}

